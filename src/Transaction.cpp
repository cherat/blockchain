/*
 * Transaction.cpp
 *
 *  Created on: Feb 6, 2019
 *      Author: C.Herat
 */
#include <iostream>
#include <string>
#include <sstream>


#include "Transaction.h"

using namespace std;

Transaction::Transaction() {
	// TODO Auto-generated constructor stub

}

Transaction::~Transaction() {
	// TODO Auto-generated destructor stub
}


//function to create a new transaction  bloc with input Tx list and output Tx list and add it to the transaction list
//this is a fifo list and we can browse in both direction in it.
transactionbloc *Transaction::AddTransactionBloc(inputTx input_tx, outputTx output_tx, transactionbloc *head_list){
	transactionbloc *new_tx_bloc=new transactionbloc;
	new_tx_bloc->list_input = input_tx;
	new_tx_bloc->list_output = output_tx;
	//if it's the first tx bloc in the tx list
	if (head_list == NULL){
		new_tx_bloc->next = NULL;
		new_tx_bloc->prev = NULL;
		current_nb_tx = 0;
		head_list = new_tx_bloc;
	}
	//in other cases
	else {
		//search last tx
		transactionbloc *last_tx = head_list;
		while(last_tx->next != NULL){
			last_tx = last_tx->next;
		}
		//add new tx to last tx
		new_tx_bloc->prev=last_tx;
		current_nb_tx ++;
		last_tx->next=new_tx_bloc;
	}
	return head_list;
}


std::string Transaction::StockInputtx(inputTx item){
	return string(item.time +";"+ item.amount);
}

std::string Transaction::StockOutputtx(outputTx item){
	return string(item.time  +";"+ item.amount);

}

inputTx *Transaction::LoadInputtx(std::string item){
	inputTx *new_inputTx = new inputTx;

    string arr[2];
    int i = 0;
    stringstream ssin(item);
    while (ssin.good() && i < 4){
        ssin >> arr[i];
        ++i;
    }
	new_inputTx->time = arr[0];
	new_inputTx->amount = arr[1];
	return new_inputTx;
}

outputTx *Transaction::LoadOutputtx(std::string item){
	outputTx *new_outputTx = new outputTx;

    string arr[2];
    int i = 0;
    stringstream ssin(item);
    while (ssin.good() && i < 4){
        ssin >> arr[i];
        ++i;
    }
	new_outputTx->time = arr[0];
	new_outputTx->amount = arr[1];
	return new_outputTx;
}


std::string Transaction::StockTransaction(transactionbloc *tx){
//    Document document;
//	// define the document as an object rather than an array
//	document.SetObject();
//	Value tmp_str;
//
//	// create a rapidjson array type with similar syntax to std::vector
//	rapidjson::Value array(rapidjson::kArrayType);
//	// must pass an allocator when the object may need to allocate memory
//	rapidjson::Document::AllocatorType& allocator = document.GetAllocator();
//
//	document.AddMember("hash", "B", allocator);
//	document.AddMember("next_hash", "C", allocator);
//	document.AddMember("previous_hash", "A", allocator);
//
//
//	// create a rapidjson object type
//	rapidjson::Value input(rapidjson::kObjectType);
//	{
//		tmp_str.SetString(StockInputtx(tx->list_input).c_str(), allocator);
//		input.AddMember("In 1", tmp_str, allocator);
//		document.AddMember("Input_list", input, allocator);
//	}
//
//
//	rapidjson::Value output(rapidjson::kObjectType);
//	{
//		tmp_str.SetString(StockOutputtx(tx->list_output).c_str(), allocator);
//		output.AddMember("Out 1", tmp_str, allocator);
//		document.AddMember("Output_list", output, allocator);
//	}
//
//	StringBuffer strbuf;
//	Writer<StringBuffer> writer(strbuf);
//	document.Accept(writer);
////	std::cout << strbuf.GetString() << std::endl;
//    return strbuf.GetString();
}


