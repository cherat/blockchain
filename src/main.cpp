//============================================================================
// Name        : BlockChain.cpp
// Author      : C.Herat
// Version     :
// Description :
//============================================================================

#include <iostream>
#include <string>
#include <limits>
#include "MerkleTree.h"
#include "Transaction.h"
#include "Block.h"


using namespace std;


void test_build_block(){
	Transaction c_transactions;
	Block c_block;

	transactionbloc *tx_list = new transactionbloc; tx_list = NULL;
	block *new_block = new block;

	//create transaction list
	for (int i= 0;i<NB_TX_MAX;i++){

			inputTx *input_tx = new inputTx;
			input_tx->amount=42.1;
			input_tx->time = "today";

			outputTx *output_tx = new outputTx;
			output_tx->amount=12.4;
			output_tx->time ="yesterday";

			tx_list = c_transactions.AddTransactionBloc(*input_tx, *output_tx, tx_list);
			c_transactions.StockTransaction(tx_list);
		}


	//build block
	new_block=c_block.BuildBlock(new_block, tx_list);



}



void test_block_class(){
	//TODO create tx list
	//add new trasaction to tx list until NB_TX_MAX is not succeed

	Transaction c_transactions;
	Block c_block;
	block *blockchain = new block;

	transactionbloc *tx_list = new transactionbloc;
	tx_list = NULL;

	//create transaction list
	for (int i= 0;i<NB_TX_MAX;i++){

			inputTx *input_tx = new inputTx;
			input_tx->amount="42.1";
			input_tx->time = "today";

			outputTx *output_tx = new outputTx;
			output_tx->amount="12.4";
			output_tx->time ="yesterday";

			tx_list = c_transactions.AddTransactionBloc(*input_tx, *output_tx, tx_list);

	}

	if(c_transactions.current_nb_tx == NB_TX_MAX-1){
		block *new_block = c_block.BuildBlock(blockchain, tx_list);
		block *valide_block = c_block.ValidateBlock(new_block, 1, numeric_limits<long>::max());


		blockchain = valide_block;
		c_block.SaveBlock(valide_block, "output.json");
	}




}



int main() {
	cout << "_____Test Block Class_____" << endl;


//	test_block_class();

	Block c_block;
	c_block.LoadBlock("toto");


	return 0;
}
