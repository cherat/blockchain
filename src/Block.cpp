/*
 * Block.cpp
 *
 *  Created on: Feb 4, 2019
 *      Author: C.Herat
 */

#include <iostream>
#include <string>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/filereadstream.h"
#include "MerkleTree.h"
#include "Transaction.h"
#include "Block.h"


using namespace rapidjson;
using namespace std;

Block::Block() {
	// TODO Auto-generated constructor stub

}

Block::~Block() {
	// TODO Auto-generated destructor stub
}


//function to create a whole bloc with complete transaction list
block *Block::BuildBlock(block *block_chain, transactionbloc *tx_list){
	MerkleTree merkletree;

	block *new_block = new block;
//	block *last_block = new block;
	//Merkletree
	new_block->root_hash = merkletree.BuildMerkleTree(tx_list);
	//TODO remove outside test
	merkletree.DumpMerkleTree(true);
	//compute hash of last block of block chain
	new_block->previous_hash = ComputeNewHash(block_chain);
	//link last block of block chain with new one
	new_block->previous_block = block_chain;

	//link tx list to block
	new_block->transactions_list = tx_list;
	//return the new block as queues of block chain
	return new_block;
}


std::string Block::ComputeNewHash(block *block_to_hash){
	//TODO complete this function to realize a true hash of last block
	return block_to_hash->root_hash;
}


bool Block::valide_hash(std::string hash){
	return true;
}


block *Block::ValidateBlock(block *new_block, long min_nonce, long max_nonce){
	long nonce = 0;
	std::string hash;
	block *validate_block = new_block;

	for(nonce=min_nonce; nonce<max_nonce; nonce++){
		validate_block->nonce=nonce;
		hash = ComputeNewHash(validate_block);
		if (valide_hash(hash))
			return validate_block;
	}
}


void Block::SaveBlock(block *validate_block, std::string filename){
	Transaction c_transactions;

	Document document;
	// define the document as an object rather than an array
	document.SetObject();
	Value tmp_str;

	// create a rapidjson array type with similar syntax to std::vector
	rapidjson::Value array(rapidjson::kArrayType);
	// must pass an allocator when the object may need to allocate memory
	rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

	//header
	tmp_str.SetString(validate_block->root_hash.c_str(), allocator);
	document.AddMember("Root Hash", tmp_str, allocator);

	tmp_str.SetString(validate_block->previous_hash.c_str(), allocator);
	document.AddMember("Previous Hash", tmp_str, allocator);

	tmp_str.SetInt(validate_block->nonce);
	document.AddMember("Nonce", tmp_str, allocator);

	//body
	rapidjson::Value json_txlist(rapidjson::kObjectType);
	{
		//loop to take all tx
		transactionbloc *tmp_tx = new transactionbloc;
		tmp_tx=validate_block->transactions_list;
		int counter = 0;

		while(tmp_tx != NULL){
			rapidjson::Value json_tx(rapidjson::kObjectType);
			{
				json_tx.AddMember("hash", "B", allocator);
				json_tx.AddMember("next_hash", "C", allocator);
				json_tx.AddMember("previous_hash", "A", allocator);

				// Input List
				rapidjson::Value input(rapidjson::kObjectType);
				{
					tmp_str.SetString(c_transactions.StockInputtx(tmp_tx->list_input).c_str(), allocator);
					input.AddMember("In 1", tmp_str, allocator);
				json_tx.AddMember("Input_list", input, allocator);
				}
				//Output list
				rapidjson::Value output(rapidjson::kObjectType);
				{
					tmp_str.SetString(c_transactions.StockOutputtx(tmp_tx->list_output).c_str(), allocator);
					output.AddMember("Out 1", tmp_str, allocator);
				json_tx.AddMember("Output_list", output, allocator);
				}

			tmp_str.SetString(to_string(counter).c_str(), allocator);
			json_txlist.AddMember(tmp_str, json_tx, allocator);
			}
			//new item of list
			tmp_tx=tmp_tx->next;
			counter++;
		}
	document.AddMember("Transaction_list", json_txlist, allocator);
	}
	//TODO change name of file by variable
	FILE* fp = fopen("output.json", "w"); // non-Windows use "w"
	char writeBuffer[65536];
	FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
//    PrettyWriter<FileWriteStream> writer(os);
	Writer<FileWriteStream> writer(os);
	document.Accept(writer);
	fclose(fp);
}


block *Block::LoadBlock(std::string filename){
	Transaction c_transactions;
	Document document;


	//putting file in memory to work on it
	FILE* fp = fopen("output.json", "r"); // non-Windows use "r"

	char readBuffer[65536];
	Value tmp_str;

	FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	document.ParseStream(is);
	fclose(fp);
	cout << readBuffer <<endl;

	//let's do it
//	cout << document["Root Hash"].GetString() <<endl;
//	cout << document["Transaction_list"]["0"]["hash"].GetString() <<endl;
	block *new_block = new block;
	new_block->root_hash=document["Root Hash"].GetString();
	new_block->previous_hash =document["Root Hash"].GetString();
	new_block->nonce=document["Nonce"].GetInt();

	transactionbloc *tx_list = NULL;
	//loop to recreate tx list
	int i =0;
	for (i=0; i<NB_TX_MAX; i++){
		//transform int in correct string for json
		tmp_str.SetString(to_string(i).c_str(), sizeof(char));
//		cout << document["Transaction_list"][tmp_str]["hash"].GetString() <<endl;
		inputTx *new_inputTx = c_transactions.LoadInputtx(document["Transaction_list"][tmp_str]["Input_list"]["In 1"].GetString());
		outputTx *new_outputTx = c_transactions.LoadOutputtx(document["Transaction_list"][tmp_str]["Output_list"]["Out 1"].GetString());
		tx_list = c_transactions.AddTransactionBloc(*new_inputTx, *new_outputTx, tx_list);
	}
	new_block->transactions_list=tx_list;
	return new_block;
}
