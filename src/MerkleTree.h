/*
 * MerkleTree.h
 *
 *  Created on: Feb 5, 2019
 *      Author: C.Herat
 */

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <random>
#include "Transaction.h"
#include "Conf.h"

#ifndef MERKLETREE_H_
#define MERKLETREE_H_


using std::vector;
using std::string;
using std::cout;


struct node
{
	string hash;
	int key_value;
	node *left;
	node *right;
};

struct cell_display {
    string   valstr;
    bool     present;
    cell_display() : present(false) {}
    cell_display(string valstr) : valstr(valstr), present(true) {}
};

using display_rows = vector<vector< cell_display > >;


class MerkleTree {
public:
	//attributs
	node *root;
	node *tab_tmp[NB_TX_MAX]; //this tab is use to not lose leafs node when they are added //TODO verify if 10 leaf max is correct

	//functions
	MerkleTree();
	virtual ~MerkleTree();
	void DestroyTree(node *leaf);
	//function to create leaf nodes level in tree
	void InsertLeafNode(node *new_leaf, transactionbloc *tx, int item);
	//create others node level in tree
	void InsertParentNode(node *new_parent, node *left, node *right, int item);
	//hash functions
	string ComputeNewHash(string hash_left,string hash_right);
	string ComputeTransactionHash(transactionbloc *hash);
	//check if a ticket is
	node *SearchTransaction(Transaction tx); //TODO implement this function

	//global function to create a merkle tree from a transactionlist
	string BuildMerkleTree(transactionbloc *list_tx);


	//functions to dump tree
    void DumpMerkleTree(bool in_file);
    int GetMaxDepth(node *root);
    display_rows GetRowDisplay();
    vector<string> RowFormatter(const display_rows& rows_disp);
    void TrimRowsLeft(vector<string>& rows);
};


#endif /* MERKLETREE_H_ */
