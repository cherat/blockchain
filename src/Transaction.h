/*
 * Transaction.h
 *
 *  Created on: Feb 6, 2019
 *      Author: C.Herat
 */

#include <string>

#ifndef TRANSACTION_H_
#define TRANSACTION_H_


struct inputTx{
	std::string time;
	std::string amount;
	inputTx *next;
};

struct outputTx{
	std::string time;
	std::string amount;
	outputTx *next;
};


struct transactionbloc{
	inputTx list_input;
	outputTx list_output;
	transactionbloc *next;
	transactionbloc *prev;
};

class Transaction {
public:
	int current_nb_tx;

	Transaction();
	//return a string in csv format of input and output list
	std::string StockInputtx(inputTx item);
	std::string StockOutputtx(outputTx item);
	inputTx *LoadInputtx(std::string item);
	outputTx *LoadOutputtx(std::string item);
	//return a json object in string type
	std::string StockTransaction(transactionbloc *tx);

	virtual ~Transaction();
	transactionbloc *AddTransactionBloc(inputTx input_tx, outputTx output_tx, transactionbloc *head_list);

};

#endif /* TRANSACTION_H_ */
