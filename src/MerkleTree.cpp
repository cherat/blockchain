/*
 * MerkleTree.cpp
 *
 *  Created on: Feb 5, 2019
 *      Author: C.Herat
 */
#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <random>
#include <openssl/sha.h>
#include "MerkleTree.h"
#include "Transaction.h"
#include "Conf.h"

using std::vector;
using std::string;
using std::cout;


//constructor
MerkleTree::MerkleTree() {
	root = NULL;

	//init tab_tmp to null to avoid memory overflow
	int i =0;
	for (i = 0; i<NB_TX_MAX; i++ )
		tab_tmp[i] = NULL;
}

MerkleTree::~MerkleTree(){
}

//destructor
void MerkleTree::DestroyTree(node *leaf) {
	//TODO check if correct
	if(leaf!=NULL)
	{
		DestroyTree(leaf->left);
		DestroyTree(leaf->right);
		delete leaf;
	}
}

//insertion of a leaf node / creation of 1st tab_tmp
void MerkleTree::InsertLeafNode(node *new_leaf, transactionbloc *tx,int item){
	new_leaf->hash = ComputeTransactionHash(tx);
	new_leaf->right=NULL;
	new_leaf->left=NULL;

	tab_tmp[item] = new_leaf; //we're increase pointer on array to save next leaf in empty case //TODO : check if correct
}

void  MerkleTree::InsertParentNode(node *new_parent, node *left, node *right,int item){
	new_parent->left=left;
	new_parent->right = right;

	if(right == NULL)
		right = left;
	new_parent->hash = ComputeNewHash(left->hash,right->hash); //calculate the new hash according to the following formula : H = hash(hash_left || hash_right) where '||' is concatenation and 'hash' a hash function
	tab_tmp[item] = new_parent;
}

string MerkleTree::BuildMerkleTree(transactionbloc *list_tx){
	//creating a pointer to browse the list without losing the head
	transactionbloc *ptr_tx_tmp = list_tx;
	int item = 0;
	//browse the list
	while(ptr_tx_tmp != NULL){
		node *new_leaf = new node;
		InsertLeafNode(new_leaf, ptr_tx_tmp, item);

		//go to next item in list
		item++;
		ptr_tx_tmp=ptr_tx_tmp->next;
	}

	//while we don't have only one ROOT node
	while (tab_tmp[1] != NULL){
		item = 0;
		int current = 0;
//		//reset pointer to browse the tab without losing the head
//		ptr_tab_tmp = tab_tmp[0];
		while((tab_tmp[item] != NULL) & (item < NB_TX_MAX)){
			node *new_parent = new node;
			InsertParentNode(new_parent, tab_tmp[item], tab_tmp[item + 1], current);
			item +=2;
			current ++;
		}

		//put the rest of tab to NULL
		while ((tab_tmp[current] != NULL)&(current<NB_TX_MAX)){
			tab_tmp[current] = NULL;
			current ++; //TODO Check if correct way to increase ptr
		}
	}
	this->root = tab_tmp[0];
	return this->root->hash;
}

string MerkleTree::ComputeNewHash(string hash_left, string hash_right){
//	unsigned char digest[SHA256_DIGEST_LENGTH];
//	std::string s (hash_left+hash_right);
//
//	SHA256((unsigned char*)&s, s.length(), (unsigned char*)&digest);
//
//	char mdString[SHA256_DIGEST_LENGTH*2+1];
//
//	for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
//		 sprintf(&mdString[i*2], "%02x", (unsigned int)digest[i]);
//
//	printf("SHA256 digest: %s\n", mdString);
//
//	return mdString;

	return hash_left + hash_right;
}

string MerkleTree::ComputeTransactionHash(transactionbloc *tx){
//	char tx_bytes[] = reinterpret_cast<char*>(&tx);
//	unsigned char digest[SHA256_DIGEST_LENGTH];
//
//	SHA256((unsigned char*)&tx_bytes, strlen(tx_bytes), (unsigned char*)&digest);
//	char mdString[SHA256_DIGEST_LENGTH*2+1];
//	for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
//		 sprintf(&mdString[i*2], "%02x", (unsigned int)digest[i]);
//
//    printf("SHA256 digest: %s\n", mdString);
//	return mdString;

	return tx->list_input.time;
}

node MerkleTree::*search_transaction(Transaction tx){
	//TODO function
}

//____________________________//
//function to dump merkle tree//
//____________________________//
void MerkleTree::DumpMerkleTree(bool in_file){
 	   std::ofstream myfile;
 	   myfile.open ("MT.txt");

 	   const int d = GetMaxDepth(MerkleTree::root);

       // If this tree is empty, tell someone
       if(d == 0) {
           cout << " <empty tree>\n"; //TODO solve this
           return;
       }

       // This tree is not empty, so get a list of node values...
       const auto rows_disp = GetRowDisplay();
       // then format these into a text representation...
       auto formatted_rows = RowFormatter(rows_disp);
       // then trim excess space characters from the left sides of the text...
       TrimRowsLeft(formatted_rows);
       // then dump the text to cout.
       for(const auto& row : formatted_rows) {
           cout << ' ' << row << '\n';
       	   myfile << ' ' << row << '\n';
       }
       myfile.close();
   }

//get max depth
int MerkleTree::GetMaxDepth(node *root){
	int max_depth = 0;
	//create tmp pointer to iterate on tree and not lose head
	node *ptr_tmp_depth=root;
	//while current next left node is not NULL, look if next one is null or not is one iteration than look for current node
	//in addition, we are going on extrem left branch of the tree,it is correct in our case because the level of the leafs nodes is homogeneous, the exploration of a single branch gives the max length of all.
	while(ptr_tmp_depth->left != NULL){
		ptr_tmp_depth=ptr_tmp_depth->left;
		max_depth++;
	}

	return (max_depth+1); //TODO : maybe +1
}

display_rows MerkleTree::GetRowDisplay() {
        // start off by traversing the tree to
        // build a vector of vectors of Node pointers
        vector<node*> traversal_stack;
        vector< vector<node*> > rows;
        if(!MerkleTree::root) return display_rows();

        node *p = MerkleTree::root;
        const int max_depth = GetMaxDepth(MerkleTree::root);
        rows.resize(max_depth);
        int depth = 0;
        for(;;) {
            // Max-depth Nodes are always a leaf or null
            // This special case blocks deeper traversal
            if(depth == max_depth-1) {
                rows[depth].push_back(p);
                if(depth == 0) break;
                --depth;
                continue;
            }

            // First visit to node?  Go to left child.
            if(traversal_stack.size() == depth) {
                rows[depth].push_back(p);
                traversal_stack.push_back(p);
                if(p) p = p->left;
                ++depth;
                continue;
            }

            // Odd child count? Go to right child.
            if(rows[depth+1].size() % 2) {
                p = traversal_stack.back();
                if(p) p = p->right;
                ++depth;
                continue;
            }

            // Time to leave if we get here

            // Exit loop if this is the root
            if(depth == 0) break;

            traversal_stack.pop_back();
            p = traversal_stack.back();
            --depth;
        }

        // Use rows of Node pointers to populate rows of cell_display structs.
        // All possible slots in the tree get a cell_display struct,
        // so if there is no actual Node at a struct's location,
        // its boolean "present" field is set to false.
        // The struct also contains a string representation of
        // its Node's value, created using a std::stringstream object.
        display_rows rows_disp;
        std::stringstream ss;
        for(const auto& row : rows) {
            rows_disp.emplace_back();
            for(node* pn : row) {
                if(pn) {
                    ss << pn->hash;
                    rows_disp.back().push_back(cell_display(ss.str()));
                    ss = std::stringstream();
                } else {
                    rows_disp.back().push_back(cell_display());
        }   }   }
        return rows_disp;
    }

// row_formatter takes the vector of rows of cell_display structs
// generated by get_row_display and formats it into a test representation
// as a vector of strings
vector<string> MerkleTree::RowFormatter(const display_rows& rows_disp){
	using s_t = string::size_type;

	// First find the maximum value string length and put it in cell_width
	s_t cell_width = 0;
	for(const auto& row_disp : rows_disp) {
		for(const auto& cd : row_disp) {
			if(cd.present && cd.valstr.length() > cell_width) {
				cell_width = cd.valstr.length();
	}   }   }

	// make sure the cell_width is an odd number
	if(cell_width % 2 == 0) ++cell_width;

	// formatted_rows will hold the results
	vector<string> formatted_rows;

	// some of these counting variables are related,
	// so its should be possible to eliminate some of them.
	s_t row_count = rows_disp.size();

	// this row's element count, a power of two
	s_t row_elem_count = 1 << (row_count-1);

	// left_pad holds the number of space charactes at the beginning of the bottom row
	s_t left_pad = 0;

	// Work from the level of maximum depth, up to the root
	// ("formatted_rows" will need to be reversed when done)
	for(s_t r=0; r<row_count; ++r) {
		const auto& cd_row = rows_disp[row_count-r-1]; // r reverse-indexes the row
		// "space" will be the number of rows of slashes needed to get
		// from this row to the next.  It is also used to determine other
		// text offsets.
		s_t space = (s_t(1) << r) * (cell_width + 1) / 2 - 1;
		// "row" holds the line of text currently being assembled
		string row;
		// iterate over each element in this row
		for(s_t c=0; c<row_elem_count; ++c) {
			// add padding, more when this is not the leftmost element
			row += string(c ? left_pad*2+1 : left_pad, ' ');
			if(cd_row[c].present) {
				// This position corresponds to an existing Node
				const string& valstr = cd_row[c].valstr;
				// Try to pad the left and right sides of the value string
				// with the same number of spaces.  If padding requires an
				// odd number of spaces, right-sided children get the longer
				// padding on the right side, while left-sided children
				// get it on the left side.
				s_t long_padding = cell_width - valstr.length();
				s_t short_padding = long_padding / 2;
				long_padding -= short_padding;
				row += string(c%2 ? short_padding : long_padding, ' ');
				row += valstr;
				row += string(c%2 ? long_padding : short_padding, ' ');
			} else {
				// This position is empty, Nodeless...
				row += string(cell_width, ' ');
			}
		}
		// A row of spaced-apart value strings is ready, add it to the result vector
		formatted_rows.push_back(row);

		// The root has been added, so this loop is finsished
		if(row_elem_count == 1) break;

		// Add rows of forward- and back- slash characters, spaced apart
		// to "connect" two rows' Node value strings.
		// The "space" variable counts the number of rows needed here.
		s_t left_space  = space + 1;
		s_t right_space = space - 1;
		for(s_t sr=0; sr<space; ++sr) {
			string row;
			for(s_t c=0; c<row_elem_count; ++c) {
				if(c % 2 == 0) {
					row += string(c ? left_space*2 + 1 : left_space, ' ');
					row += cd_row[c].present ? '/' : ' ';
					row += string(right_space + 1, ' ');
				} else {
					row += string(right_space, ' ');
					row += cd_row[c].present ? '\\' : ' ';
				}
			}
			formatted_rows.push_back(row);
			++left_space;
			--right_space;
		}
		left_pad += space + 1;
		row_elem_count /= 2;
	}

	// Reverse the result, placing the root node at the beginning (top)
	std::reverse(formatted_rows.begin(), formatted_rows.end());

	return formatted_rows;
}

// Trims an equal number of space characters from
// the beginning of each string in the vector.
// At least one string in the vector will end up beginning
// with no space characters.
void MerkleTree::TrimRowsLeft(vector<string>& rows) {
	if(!rows.size()) return;
	auto min_space = rows.front().length();
	for(const auto& row : rows) {
		auto i = row.find_first_not_of(' ');
		if(i==string::npos) i = row.length();
		if(i == 0) return;
		if(i < min_space) min_space = i;
	}
	for(auto& row : rows) {
		row.erase(0, min_space);
	}
}



