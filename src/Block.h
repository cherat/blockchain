/*
 * Block.h
 *
 *  Created on: Feb 4, 2019
 *      Author: C.Herat
 */

#include <string>
#include "Transaction.h"

#ifndef BLOCK_H_
#define BLOCK_H_


struct block{
	long nonce;
	std::string previous_hash ;
	std::string root_hash;
	transactionbloc *transactions_list;
	block *previous_block;
//	block *next_block; //TODO : maybe useless ?
};


class Block {
public:
	Block();
	virtual ~Block();

	block *BuildBlock(block *new_block, transactionbloc *transactions_list);
	//compute hash for the whole bloc
	std::string ComputeNewHash(block *block_to_hash);

	//function to validate the block by fouding the correct nonce
	block *ValidateBlock(block *new_block, long min_nonce, long max_nonce);
	//function to check if the hash is correct or not
	bool valide_hash(std::string hash);

	void SaveBlock(block *validate_block, std::string filename);
	block *LoadBlock(std::string filename);

};

#endif /* BLOCK_H_ */
